import unittest
import app

class TestGitci(unittest.TestCase):
    def test_add(self):
        inc = app.Gitci()
        final = inc.add(2,3)
        self.assertEqual(int(final), 5)

    def test_sub(self):
        inc = app.Gitci()
        final = inc.sub(3,2)
        self.assertEqual(int(final), 1)

if __name__ == "__main__":
    unittest.main()
