from flask import Flask,request
import test_app

app = Flask(__name__)

class Gitci:
    def index(self):
        return 'hello bob from user065'
    def add(self,x, y):
        return str(x+y)
    def sub(self,x,y):
	    return str(x-y)

instance = Gitci()

@app.route('/')
def index():
    return index()

@app.route('/add', methods=['GET'])
def add():
    return instance.add(int(request.args.get('a')),int(request.args.get('b')))

@app.route('/sub', methods=['GET'])
def sub():
    return instance.sub(int(request.args.get('a')),int(request.args.get('b')))

if __name__=="__main__":
    app.run(host='0.0.0.0', port=8065)


